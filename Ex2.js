let valor = 40

if (valor < 25) {
    console.log("O valor está no intervalo 0 ~ 25")
} else if (valor >= 25 && valor < 50){
    console.log("O valor está no intervalo 25 ~ 50")

} else if (valor >= 50 && valor < 75){
    console.log("O valor está no intervalo 50 ~ 75")

} else if (valor >= 75 && valor <= 100){
    console.log("O valor está no intervalo 75 ~ 100")

} else {
    console.log("Valor fora do intervalo!")
}