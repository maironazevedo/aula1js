let segundos = 74050

let horas = (segundos / 3600)
let minutos = (segundos % 3600) / 60
let segundosRestantes = (segundos % 3600) % 60

horas = Math.floor(horas)
minutos = Math.floor(minutos)
segundosRestantes = Math.floor(segundosRestantes)

let resultado = horas + " Horas, " + minutos + " minutos e " + segundosRestantes + " segundos."

console.log(resultado)